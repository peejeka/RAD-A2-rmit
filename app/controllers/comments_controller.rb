class CommentsController < ApplicationController
	def index
		@comments = Comment.order('created_at DESC').limit(3).offset(0)
	end

	def create
		@article = Article.find(params[:article_id])
		@comment = @article.comments.create(comment_params)
     if @comment.valid?
			flash[:notice] = 'Successfully add comment'
			redirect_to '/newcomments'
		else
			flash[:notice] = 'Comment should not be blank and within 3-999 characters'
			redirect_back fallback_location: root_path
	   end
	end
	
	def showSingular(curTime, singularStr, multiStr)
		if curTime.to_i <= 1
			curTime + ' ' + singularStr
		else
			curTime + ' ' + multiStr
		end
	end
	
	def showTime(curTime)
		timeDiff = Time.now - curTime
		if timeDiff < 60
			showSingular(timeDiff.to_i.to_s, 'sec', 'secs')
		else
			timeDiff = (timeDiff / 60).to_i
			if timeDiff < 60
				showSingular(timeDiff.to_s, 'min', 'mins')
			else
				timeDiff = (timeDiff / 60).to_i
				if timeDiff < 24
					showSingular(timeDiff.to_s, 'hour', 'hours')
				else
					timeDiff = (timeDiff / 24).to_i
					if timeDiff < 30
						showSingular(timeDiff.to_s, 'day', 'days')
					else
						timeDiff = (timeDiff / 30).to_i
						if timeDiff < 12
							showSingular(timeDiff.to_s, 'month', 'months')
						else
							timeDiff = (timeDiff / 12).to_i
							showSingular(timeDiff.to_s, 'year', 'years')
						end
					end
				end
			end
		end
	end
	helper_method :showTime
	
	private
    def comment_params
      params.require(:comment).permit(:content)
    end
end
