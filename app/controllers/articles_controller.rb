class ArticlesController < ApplicationController
	skip_before_action :verify_authenticity_token

	def index
		curLimit = 8
		@curPage = 0
		@curOffset = 0
		if params[:p]
			@curPage = params[:p].to_i
			@curOffset = @curPage * curLimit
		end
		@articles = Article.order('created_at DESC')
		if (@curOffset + curLimit) < @articles.count
			@morePage = 1
		end
		@articles = @articles.limit(curLimit).offset(@curOffset)
	end
	
	def show
		@article = Article.find(params[:id])
	end

	def new
		if session[:account_id]
			@account = Account.find(session[:account_id])
		else
			redirect_to '/login'
		end
	end
	
	def create
		@account = Account.find(params[:account_id])
    @article = @account.articles.create(article_params)
		if @article.valid?
			flash[:notice] = 'Successfully create news'
			redirect_to '/submit'
		else
			flash[:notice] = 'News should not be blank and within 10-199 characters. URL must be valid.'
			redirect_back fallback_location: root_path
		end
	end
	
	def apicreate
		status = :bad_request
		json = JSON.parse(request.body.read)
		if json.has_key?('username') and json.has_key?('text') and json.has_key?('type')
			account = Account.find_by(username: json['username'].downcase)
			if account
				if json['type'] == 'news'
					if json.has_key?('new source')
						account.articles.create(content: json['text'], source: json['new source'])
					else
						account.articles.create(content: json['text'])
					end
					status = :ok
				end
			end
		end
		render nothing: true, status: status
	end
	
	def apishow
		status = :bad_request
		strlen = params['name'].length - 1
		if strlen > 0
			@type = params['name'][0]
			@id = params['name'][1..strlen].to_i
			if @type == 'n' or @type == 'c'
				if @type == 'n'
					@article = Article.find(@id)
					@result = {:by => @article.account.username, :id => @article.id, :time => @article.created_at.to_i, :title => @article.content, :type => 'news', :url => @article.source}
				else
					@comment = Comment.find(@id)
					@result = {:by => @comment.article.account.username, :id => @comment.id, :text => @comment.content, :time => @comment.created_at.to_i, :type => 'comment'}
				end
				status = :ok
			end
		end
		render nothing: true, status: status
	end
	
	def getSourceLink(curSource)
		result = curSource.dup
		unless result.include? 'http://' or result.include? 'https://'
			result.prepend('http://')
		end
		result
	end
	helper_method :getSourceLink
	
	def showSource(curSource)
		newSource = getSourceLink(curSource)
		URI.parse(newSource).host
	end
	helper_method :showSource
	
	def showSingular(curTime, singularStr, multiStr)
		if curTime.to_i <= 1
			curTime + ' ' + singularStr
		else
			curTime + ' ' + multiStr
		end
	end
	
	def showTime(curTime)
		timeDiff = Time.now - curTime
		if timeDiff < 60
			showSingular(timeDiff.to_i.to_s, 'sec', 'secs')
		else
			timeDiff = (timeDiff / 60).to_i
			if timeDiff < 60
				showSingular(timeDiff.to_s, 'min', 'mins')
			else
				timeDiff = (timeDiff / 60).to_i
				if timeDiff < 24
					showSingular(timeDiff.to_s, 'hour', 'hours')
				else
					timeDiff = (timeDiff / 24).to_i
					if timeDiff < 30
						showSingular(timeDiff.to_s, 'day', 'days')
					else
						timeDiff = (timeDiff / 30).to_i
						if timeDiff < 12
							showSingular(timeDiff.to_s, 'month', 'months')
						else
							timeDiff = (timeDiff / 12).to_i
							showSingular(timeDiff.to_s, 'year', 'years')
						end
					end
				end
			end
		end
	end
	helper_method :showTime
	
	private
    def article_params
      params.require(:article).permit(:content, :source)
    end
end
