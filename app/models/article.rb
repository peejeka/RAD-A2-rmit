class HttpUrlValidator < ActiveModel::EachValidator

  def self.compliant?(value)
    uri = URI.parse(value)
    uri.is_a?(URI::HTTP) && !uri.host.nil?
  rescue URI::InvalidURIError
    false
  end

  def validate_each(record, attribute, value)
		if value.present?
			unless self.class.compliant?(value)
				record.errors.add(attribute, "not a valid HTTP URL")
			end
		end
  end

end

class Article < ApplicationRecord
  belongs_to :account
	has_many :comments, -> { order(created_at: :desc) }
	validates_associated :comments
	
	validates :content, presence: true, length: { in: 10..199 }
	validates :source, presence: false, http_url: true
end
