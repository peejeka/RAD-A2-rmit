class Account < ApplicationRecord
  has_many :articles
	has_secure_password
	
	PASSWORD_FORMAT = /\A(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[[:^alnum:]])/x #At least need a digit, a small character, a capital character, a symbol character
	
	validates :username, presence: true, length: { in: 3..14 }, format: { with: /\A[a-zA-Z0-9_\-]+\Z/ }
	validates :password, presence: true, length: { minimum: 10 }, format: { with: PASSWORD_FORMAT }
end
